#include "GameObject.h"
#include "IBehaviour.h"
#include <Renderer2D.h>

GameObject::GameObject() {
	Start();
}

GameObject::~GameObject() {
	End();
	
	for (size_t i = 0; i < m_behaviours.size(); i++) {
		delete m_behaviours[i];
	}
}

void GameObject::Start() {
	for (size_t i = 0; i < m_behaviours.size(); i++) {
		m_behaviours[i]->Start();
	}
}

void GameObject::OnActive() {
	for (size_t i = 0; i < m_behaviours.size(); i++) {
		m_behaviours[i]->OnActive();
	}
}

void GameObject::Update(float deltaTime) {
	if (m_isActive) {
		for (size_t i = 0; i < m_behaviours.size(); i++) {
			m_behaviours[i]->Update(deltaTime);
		}
	}
}

void GameObject::Draw(aie::Renderer2D* renderer){
	renderer->drawCircle(m_transform.GetGlobalPosition().x, m_transform.GetGlobalPosition().y, 2.0f);

	for (size_t i = 0; i < m_behaviours.size(); i++) {
		m_behaviours[i]->Draw(renderer);
	}
}

void GameObject::End() {
	for (size_t i = 0; i < m_behaviours.size(); i++) {
		m_behaviours[i]->End();
	}
}

void GameObject::SetActive(bool state) {
	if (state) {
		OnActive();
	}

	m_isActive = state;
}

void GameObject::AddBehaviour(IBehaviour* bev) {
	bev->SetOwner(this); 
	m_behaviours.push_back(bev);
}