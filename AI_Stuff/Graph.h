#pragma once
#include <vector>

template <typename NodeType, typename EdgeType>
class Graph {
public:
	struct Node;
	struct Edge;

	struct Node {
		NodeType data;
		std::vector<Edge> edges;
	};

	struct Edge {
		Node* to;
		EdgeType data;
	};

	unsigned int AddNode(const NodeType& data) {
		Node* node = new Node();
		node->data = data;

		m_nodes.push_back(node);
		return m_nodes.size() - 1;
	}

	void AddEdge(unsigned int from, unsigned int to, bool biDir, const EdgeType& data) {
		Node* a = GetNode(from);
		Node* b = GetNode(to);

		AddEdge(a, b, biDir, data);
	}

	void AddEdge(Node* from, Node* to, bool biDir, const EdgeType& data) {
		Edge a = Edge();
		a.to = to;
		a.data = data;

		from->edges.push_back(a);

		if (biDir) {
			Edge b = Edge();
			b.to = from;
			b.data = data;
			
			to->edges.push_back(b);
		}
	}

	Node* GetNode(unsigned int i) {
		return m_nodes[i].get();
	}

	const std::vector<Node*> GetNodes() {
		return m_nodes;
	}

private:
	std::vector<Node*> m_nodes;
};