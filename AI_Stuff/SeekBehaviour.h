#pragma once
#include "IBehaviour.h"

class Transform;

class SeekBehaviour : public IBehaviour {
public:
	SeekBehaviour();
	~SeekBehaviour();

	virtual void Update(float deltaTime) override;

	void SetTarget(GameObject* object);
	void SetForce(float force);

private:
	Transform* m_targetTransform;
	float m_force;
};

