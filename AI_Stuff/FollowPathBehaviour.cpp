#include "FollowPathBehaviour.h"
#include "Path.h"
#include "GameObject.h"
#include <Vector2.h>
#include <Renderer2D.h>

FollowPathBehaviour::FollowPathBehaviour() {

}

FollowPathBehaviour::~FollowPathBehaviour() {

}

void FollowPathBehaviour::Update(float deltaTime) {
	if (!m_path->GetPath().empty()) {
		Vector2 point = m_path->GetPath()[m_currentNode];

		float dist = (point - m_owner->GetTransform().GetGlobalPosition()).magnitude();
		
		if (dist < 20.0f) {
			m_currentNode++;
		}

		m_currentNode = m_currentNode % m_path->GetPath().size();
		point = m_path->GetPath()[m_currentNode];
		
		Vector2 direction = (point - m_owner->GetTransform().GetGlobalPosition()).normal();

		m_owner->GetTransform().Translate(direction * m_force);
	}
}

void FollowPathBehaviour::Draw(aie::Renderer2D* renderer) {
	for (size_t i = 0; i < m_path->GetPath().size(); i++) {
		Vector2 point = m_path->GetPath()[i];
		renderer->drawCircle(point.x, point.y, 4.0f);

		if (i) {
			renderer->drawLine(point.x, point.y, m_path->GetPath()[i].x, m_path->GetPath()[i].y);
		}
	}
}