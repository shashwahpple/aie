#include "SeekBehaviour.h"
#include "Transform.h"
#include "GameObject.h"
#include <Vector2.h>


SeekBehaviour::SeekBehaviour() {

}

SeekBehaviour::~SeekBehaviour() {

}

void SeekBehaviour::Update(float deltaTime) {
	Vector2 direction = (m_owner->GetTransform().GetGlobalPosition() - m_targetTransform->GetGlobalPosition()).normal();
	m_owner->GetTransform().Translate(direction * m_force);
}

void SeekBehaviour::SetTarget(GameObject* object) {
	m_targetTransform = &(object->GetTransform());
}

void SeekBehaviour::SetForce(float force) {
	m_force = force;
}