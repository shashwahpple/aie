#pragma once
#include "IBehaviour.h"

class Path;

class FollowPathBehaviour : public IBehaviour {
public:
	FollowPathBehaviour();
	~FollowPathBehaviour();

	virtual void Update(float deltaTime) override;
	virtual void Draw(aie::Renderer2D* renderer) override;
	
	Path* GetPath() { return m_path; }
	void SetPath(Path* path) { m_path = path; }
	void SetForce(float f) { m_force = f; }

protected:
	float m_force = 10.0f;
	Path* m_path;
	int m_currentNode = 0;
};