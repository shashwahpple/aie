#pragma once
#include "Transform.h"
#include <vector>

namespace aie {
	class Renderer2D;
}

class IBehaviour;

class GameObject {
public:
	GameObject();
	virtual ~GameObject();

	virtual void Start();
	virtual void OnActive();
	virtual void Update(float deltaTime);
	virtual void Draw(aie::Renderer2D* renderer);
	virtual void End();

	void SetActive(bool state);

	void AddBehaviour(IBehaviour* bev);
	Transform& GetTransform() { return m_transform; }

protected:
	bool m_isActive = true;
	Transform m_transform;
	std::vector<IBehaviour*> m_behaviours;
};