#include "SpriteBehaviour.h"
#include "Transform.h"
#include "GameObject.h"
#include <Texture.h>
#include <Renderer2D.h>


SpriteBehaviour::SpriteBehaviour() {

}


SpriteBehaviour::~SpriteBehaviour() {

}

void SpriteBehaviour::Draw(aie::Renderer2D* renderer) {
	renderer->drawSpriteTransformed3x3(m_texture, m_owner->GetTransform().GetTransform().m, m_texture->getWidth(), m_texture->getHeight(), m_depth, m_offset.x, m_offset.y);
}