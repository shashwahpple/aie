#pragma once
#include <vector>
#include <Vector2.h>

class Path {
public:
	Path();
	~Path();

	void PushSegment(const Vector2& point) { m_pathPoints.push_back(point); }
	void PopSegment();
	void ClearPath() { m_pathPoints.clear(); }
	
	std::vector<Vector2>& GetPath() { return m_pathPoints; }

protected:
	std::vector<Vector2> m_pathPoints;
};