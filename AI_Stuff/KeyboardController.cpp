#include "KeyboardController.h"
#include <Input.h>
#include "GameObject.h"

void KeyboardController::Update(float deltaTime) {
	aie::Input* input = aie::Input::getInstance();
	
	if (input->isKeyDown(aie::INPUT_KEY_A)) {
		m_owner->GetTransform().Translate(Vector2(-10.0f, 0.0f) * deltaTime);
	} else if (input->isKeyDown(aie::INPUT_KEY_D)) {
		m_owner->GetTransform().Translate(Vector2(10.0f, 0.0f) * deltaTime);
	}

	if (input->isKeyDown(aie::INPUT_KEY_W)) {
		m_owner->GetTransform().Translate(Vector2(0.0f, 10.0f) * deltaTime);
	} else if (input->isKeyDown(aie::INPUT_KEY_S)) {
		m_owner->GetTransform().Translate(Vector2(0.0f, -10.0f) * deltaTime);
	}
}