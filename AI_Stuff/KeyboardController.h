#pragma once
#include "IBehaviour.h"

class KeyboardController : public IBehaviour {
public:
	virtual void Update(float deltaTime) override;
};