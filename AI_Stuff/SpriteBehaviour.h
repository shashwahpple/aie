#pragma once
#include "IBehaviour.h"
#include <Vector2.h>

namespace aie{
	class Texture;
}

class SpriteBehaviour : public IBehaviour {
public:
	SpriteBehaviour();
	~SpriteBehaviour();

	virtual void Draw(aie::Renderer2D* renderer) override;

	void SetTexture(aie::Texture* texture) { m_texture = texture; }
	void SetOffset(Vector2& vec) { m_offset = vec; }
	void SetDepth(float depth) { m_depth = depth; }

private:
	aie::Texture* m_texture;
	Vector2 m_offset = Vector2(0.5f, 0.5f);
	float m_depth = 0.0f;
};