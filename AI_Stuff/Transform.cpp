#include "Transform.h"
#include <Renderer2D.h>
#include <math.h>
#include <Utility.h>
#define MOVESPEED 100.0f

Transform::Transform() : m_direction(0.0f, 0.0f) {

}

Transform::~Transform() {

}

//	Basic update function moving the Transform in a direction
void Transform::Update(float dt) {
	Translate(m_direction * MOVESPEED * dt);
}

//	Renders a box at the global position of the Transform
void Transform::Render(aie::Renderer2D *renderer) {
	Matrix3 gTransform = CalculateGlobalTransform();
	Vector2 pos = gTransform.getTranslation();
	float rot = gTransform.getRotationZ();

	renderer->drawBox(pos.x, pos.y, 10, 20, rot);
}

//	Sets the parent of the Transform to another Transform
void Transform::SetParent(Transform *parent) {
	m_parent = parent;
}

//	Gets the local Matrix 3
Matrix3& Transform::GetTransform() {
	return m_local;
}

//	Calculates the global Matrix 3 based on matrix hierarchies and returns it
Matrix3 Transform::CalculateGlobalTransform() const {
	if (m_parent == nullptr)
		return m_local;

	return m_parent->CalculateGlobalTransform() * m_local;
}

//	Translates the Transform locally
void Transform::Translate(const Vector2 &pos) {
	Matrix3 translation = Matrix3::createTranslation(pos.x, pos.y);
	m_local = m_local * translation;
}

//	Sets the local rotation of the Transform
void Transform::Rotate(float radians) {
	Matrix3 rotation = Matrix3::createRotation(radians);
	m_local = m_local * rotation;
}

//	Gets the local Matrix 3's Translation as a Vector 2
Vector2 Transform::GetLocalPosition() {
	return m_local.getTranslation();
}


//	Returns the local rotation as a float
float Transform::GetLocalRotation() {
	return m_local.getRotationZ();
}

//	Returns the global position as a Vector 2
Vector2 Transform::GetGlobalPosition() {
	return CalculateGlobalTransform().getTranslation();
}

//	Returns the global rotation as a float
float Transform::GetGlobalRotation() {
	return CalculateGlobalTransform().getRotationZ();
}

//	Determines if two Transforms have collided assuming that they are both circles
//	Returns true if they're colliding
//	Checks if their distance apart is less than their combined radii
bool Transform::CollideWithCircle(Transform& a_trans) {
	float distance = shash::disatance(GetGlobalPosition(), a_trans.GetGlobalPosition());

	if (distance < (m_radius + a_trans.GetRadius())) {
		m_direction = -(a_trans.GetGlobalPosition() - GetGlobalPosition()).normal();

		return true;
	}

	return false;
}