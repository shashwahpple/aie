#pragma once
#include <Matrix3.h>
#include <Vector2.h>

//	 //
//	<* \__/
//	 \___/
//	  _|
//	Jack Hunt

namespace aie {
	class Renderer2D;
}


class Transform
{
public:
	Transform();
	virtual ~Transform();

	virtual void Update(float dt);
	virtual void Render(aie::Renderer2D *renderer);

	void SetParent(Transform *parent);

	Matrix3& GetTransform();
	Matrix3 CalculateGlobalTransform() const;

	void Translate(const Vector2 &pos);
	void Rotate(float radians);

	Vector2 GetLocalPosition();
	float GetLocalRotation();
	void setLocalMatrix(float p_radians) { m_local.setRotateZ(p_radians); }

	float GetRadius() { return m_radius; }

	bool CollideWithCircle(Transform& a_trans);

	// WARNING:
	// Expensive -- does a lot of things
	Vector2 GetGlobalPosition();
	float GetGlobalRotation();

	void SetDirection(Vector2& a_vec) { m_direction = a_vec; }
protected:
	float m_radius = 32.0f;
	Matrix3 m_local; // defaults to identity 
	Transform *m_parent = nullptr;
	Vector2 m_direction;
private:

};

