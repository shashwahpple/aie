#pragma once

namespace aie {
	class Renderer2D;
}

class GameObject;

class IBehaviour {
public:
	virtual void Start() {}
	virtual void OnActive() {}
	virtual void Update(float deltaTime) {}
	virtual void Draw(aie::Renderer2D* renderer){}
	virtual void End() {}

	void SetOwner(GameObject* object) { m_owner = object; }

protected:
	GameObject* m_owner;
};
