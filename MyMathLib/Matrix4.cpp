#include "Matrix4.h"

#include <memory.h>
#include <math.h>

Matrix4::~Matrix4() {

}

Matrix4::Matrix4() {
	*this = createIdentity();
}

void  Matrix4::set(float *a_ptr) {
	memcpy(v, a_ptr, sizeof(float) * 16);
}

Matrix4 Matrix4::createIdentity() {
	return Matrix4( 1.0f, 0.0f, 0.0f, 0.0f,
					0.0f, 1.0f, 0.0f, 0.0f,
					0.0f, 0.0f, 1.0f, 0.0f,
					0.0f, 0.0f, 0.0f, 1.0f);
}

void  Matrix4::set(float a_m1, float a_m2, float a_m3, float a_m4, 
	float a_m5, float a_m6, float a_m7, float a_m8, 
	float a_m9, float a_m10, float a_m11, float a_m12,
	float a_m13, float a_m14, float a_m15, float a_m16)
{
	m1 = a_m1; m2 = a_m2; m3 = a_m3; m4 = a_m4; 
	m5 = a_m5; m6 = a_m6; m7 = a_m7; m8 = a_m8; 
	m9 = a_m9; m10 = a_m10; m11 = a_m11; m12 = a_m12;
	m13 = a_m13; m14 = a_m14; m15 = a_m15; m16 = a_m16;
}


Matrix4::Matrix4(float *a_ptr) {
	memcpy(m, a_ptr, sizeof(float) * 16);
}

Matrix4::Matrix4(float a_m1, float a_m2, float a_m3, float a_m4, float a_m5, float a_m6, float a_m7, float a_m8, float a_m9, float a_m10, float a_m11, float a_m12,
	float a_m13, float a_m14, float a_m15, float a_m16) :
	m1 ( a_m1), m2 (a_m2),  m3 (a_m3),  m4 (a_m4), 
	m5 ( a_m5), m6 (a_m6),  m7 (a_m7),  m8 (a_m8), 
	m9 ( a_m9), m10(a_m10), m11(a_m11), m12(a_m12),
	m13(a_m13), m14(a_m14), m15(a_m15), m16(a_m16)
{
}

Vector4 Matrix4::operator * (const Vector4 &a_vec) const {
	float x, y, z, w;

	x = m1 * a_vec.x + m5 * a_vec.y + m9  * a_vec.z + m13 * a_vec.w;
	y = m2 * a_vec.x + m6 * a_vec.y + m10 * a_vec.z + m14 * a_vec.w;
	z = m3 * a_vec.x + m7 * a_vec.y + m11 * a_vec.z + m15 * a_vec.w;
	w = m4 * a_vec.x + m8 * a_vec.y + m12 * a_vec.z + m16 * a_vec.w;

	return Vector4(x, y, z, w);
}

Matrix4  Matrix4::operator *(const Matrix4 &a_rhs) const {
	Matrix4 tmp;

	for (auto i = 0; i < 4; i++) {
		for (auto j = 0; j < 4; j++) {
			tmp.m_floats[i][j] = (	m_floats[0][j] * a_rhs.m_floats[i][0] +
									m_floats[1][j] * a_rhs.m_floats[i][1] +
									m_floats[2][j] * a_rhs.m_floats[i][2] +
									m_floats[3][j] * a_rhs.m_floats[i][3]);
		}
	}

	return tmp;
}


void Matrix4::setRotateX(float a_rot) {
	m_floats[1][1] = cosf(a_rot);
	m_floats[2][1] = -sinf(a_rot);
	m_floats[1][2] = sinf(a_rot);
	m_floats[2][2] = cosf(a_rot);
}

void Matrix4::setRotateY(float a_rot) {
	m_floats[0][0] = cosf(a_rot);
	m_floats[2][0] = sinf(a_rot);
	m_floats[0][2] = -sinf(a_rot);
	m_floats[2][2] = cosf(a_rot);
}

void Matrix4::setRotateZ(float a_rot) {
	m_floats[0][0] = cosf(a_rot);
	m_floats[1][0] = -sinf(a_rot);
	m_floats[0][1] = sinf(a_rot);
	m_floats[1][1] = cosf(a_rot);
}
